/*
    1. Declare 3 variables without initialization called username,password and role.
*/
let username;
let password;
let role;
/*
    
    2. Create a login function which is able to prompt the user to provide their username, password and role.
        -use prompt() and update the username,password and role global variable with the prompt() returned values.
        -add an if statement to check if the the username is an empty string or null or if the password is an empty string or null or if the role is an empty string or null.
            -if it is, show an alert to inform the user that their input should not be empty.
        -add an else statement. Inside the else statement add a switch to check the user's role add 3 cases and a default:
                -if the user's role is admin, show an alert with the following message:
                    "Welcome back to the class portal, admin!"
                -if the user's role is teacher, show an alert with the following message:
                    "Thank you for logging in, teacher!"
                -if the user's role is a rookie, show an alert with the following message:
                    "Welcome to the class portal, student!"
                -if the user's role does not fall under any of the cases, as a default, show a message:
                    "Role out of range."
*/
function userLogin() {
  username = prompt("Enter User Name: ");
  password = prompt("Enter Password: ");
  role = prompt("Enter Role: ");

  if (
    username === "" ||
    null ||
    password === "" ||
    null ||
    role === "" ||
    null
  ) {
    alert("Input should not be empty!");
  } else {
    switch (role) {
      case "admin":
        alert("Welcome back to the class portal, admin!");
        break;
      case "teacher":
        alert("Thank you for loggin in, teacher!");
        break;
      case "rookie":
        alert("Welcome to the class portal, student!");
        break;
      default:
        alert("Role out of range");
        break;
    }
  }
  return role;
}

userLogin();

function showAverageLetter(grade1, grade2, grade3, grade4) {
  if (role === "admin" || role === "teacher") {
    alert(role + "! " + "You are not allowed to access this feature!");
  } else if (role == "rookie") {
    let average = Math.round((grade1 + grade2 + grade3 + grade4) / 4);

    if (average <= 74) {
      console.log(
        "Hello, student, your average is " +
          average +
          "." +
          " The letter equivalent is F."
      );
    } else if (average >= 75 && average <= 79) {
      console.log(
        "Hello, student, your average is " +
          average +
          "." +
          " The letter equivalent is D."
      );
    } else if (average >= 80 && average <= 84) {
      console.log(
        "Hello, student, your average is " +
          average +
          "." +
          " The letter equivalent is C."
      );
    } else if (average >= 85 && average <= 89) {
      console.log(
        "Hello, student, your average is " +
          average +
          "." +
          " The letter equivalent is B."
      );
    } else if (average >= 90 && average <= 95) {
      console.log(
        "Hello, student, your average is " +
          average +
          "." +
          " The letter equivalent is A."
      );
    } else if (average >= 96) {
      console.log(
        "Hello, student, your average is " +
          average +
          "." +
          " The letter equivalent is A+."
      );
    }
  }
}

showAverageLetter(71, 70, 73, 74);
showAverageLetter(75, 75, 76, 78);
showAverageLetter(80, 81, 82, 78);
showAverageLetter(84, 85, 87, 88);
showAverageLetter(89, 90, 91, 90);
showAverageLetter(91, 96, 97, 99);
